FROM registry.gitlab.com/spiculedata/saiku4/saiku4-webframework/build:latest

FROM registry.gitlab.com/spiculedata/saiku4/saiku4-server-java/build:latest

FROM tiangolo/uwsgi-nginx-flask:python3.6

# install services
RUN echo "Europe/Berlin" > /etc/timezone && \
    dpkg-reconfigure tzdata && \
    apt-get -y update && \
    apt-get -y upgrade && \
    apt-get -y install \
        rabbitmq-server default-jdk

# configure rabbitmq
RUN echo "[{rabbit, [{loopback_users, []}]}]." > /etc/rabbitmq/rabbitmq.config && \
    service rabbitmq-server start && \
    rabbitmq-plugins enable rabbitmq_management && \
    service rabbitmq-server stop

# runtime configuration
#ENTRYPOINT service rabbitmq-server start && while true; do sleep 1d; done

# expose ports
EXPOSE 5672
EXPOSE 15672

WORKDIR /app
COPY /src /app
COPY ./requirements.txt /app
COPY --from=0 /usr/src/app/saiku-ui/build /app/build/
COPY --from=1 /saiku-backend /
COPY nginx.partial /etc/nginx2/conf.d/
RUN pip install -r /app/requirements.txt
COPY start.sh /start.sh
RUN chmod +x /start.sh

EXPOSE 80

ENV REPOSITORY_DIR /data

CMD ["/start.sh"]
