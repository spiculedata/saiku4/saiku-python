import pickle
import hashlib
import hmac
from os import path
from datetime import datetime
from datetime import timedelta

LICENSE_FILE = './license.lic'
LICENSE_SECRET = '_=S@1kU!=_'


class SaikuLicenseException(Exception):
    message = 'Licensing Exception'


class SaikuLicenseWrongDigestException(SaikuLicenseException):
    message = 'Wrong digest while deserializing the license file'


class License(object):
    def __init__(self, email, expiration, license_number, license_type, name,
                 user_limit, company=''):
        self.email = email
        self.expiration = expiration
        self.license_number = license_number
        self.key = license_number
        self.license_type = license_type
        self.name = name
        self.user_limit = user_limit
        self.company = company

    def to_json_data(self):
        return {
            "email": self.email,
            "expiration": self.expiration,
            "key": self.key,
            "licenseType": self.license_type,
            "name": self.name,
            "userLimit": self.user_limit,
            "company": self.company
        }


def make_digest(message):
    return hmac.new(str.encode(LICENSE_SECRET), message, hashlib.sha1).hexdigest()


def save_license(license_object, license_name, license_company, license_filename=LICENSE_FILE):
    file = open(license_filename, 'wb')
    data = {
        'digest': make_digest(pickle.dumps(license_object)),
        'license': license_object,
        'name': license_name,
        'company': license_company
    }
    pickle.dump(data, file)
    file.close()


def create_trial_license():
    expiration_millis = 1576368000000
    # trial_lic = License("john.doe@email.com", expiration_millis, "7ITQZ-DSSWM-GJPV3-L9ZNN-VQRA6-23QCA-A9AEA-6HSDK-DTLBJ-ESV6E-JPWFQ", "Saiku 4 Enterprise Edition - Trial", "John Doe", "3")
    save_license("GTDSS-BWWCI-W6VIE-9QQYR-ZA32J", "t b", "spicule")


def create_full_license(data, name, company):
    # expiration_millis = 1576368000000
    # trial_lic = License("admin.doe@email.com", expiration_millis, "7ITQZ-DSSWM-GJPV3-L9ZNN-VQRA6-23QCA-A9AEA-6HSDK-DTLBJ-ESV6E-JPWFQ", "Saiku 4 Enterprise Edition", "Admin Doe", "3")
    # save_license(trial_lic)
    save_license(data, name, company)


def fetch_license(license_filename=LICENSE_FILE):
    if not path.exists(license_filename):
        #create_trial_license()
        return {"key": None, "company":None, "name":None}

    file = open(license_filename, 'rb')
    data = pickle.load(file)
    file.close()
    new_digest = make_digest(pickle.dumps(data['license']))
    if new_digest != data['digest']:
        raise SaikuLicenseWrongDigestException()
    try:
        return {"key": data['license'], "name": data['name'], "company": data['company']}
    except:
        print("Could not decode license")


if __name__ == '__main__':
    print(fetch_license().email)
