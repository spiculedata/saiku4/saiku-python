import os
import json
from functools import total_ordering, reduce
from pprint import pprint
import re
DEFAULT_ACL_FILE = 'acl.json'
ALL_OWNER = 'all'


class AclType(object):
    PRIVATE = "PRIVATE"
    SECURED = "SECURED"
    PUBLIC = "PUBLIC"


@total_ordering
class AclMethod(object):
    def __init__(self, value: int, name: str):
        self.value = value
        self.name = name

    def __eq__(self, other):
        return self.value == other.value

    def __ne__(self, other):
        return self.value != other.value

    def __lt__(self, other):
        return self.value < other.value


AclMethod.NONE = AclMethod(0, 'NONE')
AclMethod.READ = AclMethod(1, 'READ')
AclMethod.WRITE = AclMethod(2, 'WRITE')
AclMethod.GRANT = AclMethod(3, 'GRANT')


def get_acl_method(name):
    if name == 'READ':
        return AclMethod.READ
    elif name == 'WRITE':
        return AclMethod.WRITE
    elif name == 'GRANT':
        return AclMethod.GRANT
    return AclMethod.NONE


class AclEntry(object):
    def __init__(self, owner: str = ALL_OWNER):
        self.owner = owner
        self.type = AclType.PUBLIC
        self.roles = {}
        self.users = {}

    def add_content_role(self, content: str, method: AclMethod, *methods):
        self.roles[content] = self.roles.get(content, []) + [method]
        for m in methods:
            self.roles[content].append(m)

    def add_user_role(self, user: str, method: AclMethod, *methods):
        self.users[user] = self.users.get(user, []) + [method]
        for m in methods:
            self.users[user].append(m)


class AclEntryEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, AclEntry):
            return obj.__dict__
        elif isinstance(obj, AclMethod):
            return obj.name
        else:
            return json.JSONEncoder.default(self, obj)


class AclEntryDecoder(json.JSONDecoder):
    def __init__(self, *args, **kwargs):
        json.JSONDecoder.__init__(self, object_hook=self.object_hook, *args, **kwargs)

    def object_hook(self, dct):
        if 'owner' in dct:
            acl = AclEntry(dct['owner'])
            acl.type = dct['type']
            for content in dct['roles']:
                for method in dct['roles'][content]:
                    acl.add_content_role(content, get_acl_method(method))
            for user in dct['users']:
                for method in dct['users'][user]:
                    acl.add_user_role(user, get_acl_method(method))
            return acl
        return dct

def getFolder(name):

    d = {"name": name, "objects": []}
    return d

def getFile(name):

    d = {"name": name}


    return d


def _create_full_default_acl():
    acl = AclEntry()
    acl.add_content_role('*', AclMethod.READ, AclMethod.WRITE, AclMethod.GRANT)
    acl.add_user_role('*', AclMethod.READ, AclMethod.WRITE, AclMethod.GRANT)
    return acl


class FilesystemContentRepository(object):
    """
    A content repository or content store is a database of digital content with an associated set of data management,
    search and access methods allowing application-independent access to the content. This class implements a filesystem
    based content repository implementation. It should load and save the content permissions data to an ACL (access
    control list) file on JSON format.
    """

    def __init__(self, basedir: str):
        self.basedir = basedir
        self.acl: AclEntry = _create_full_default_acl()
        self.load_acl_file(DEFAULT_ACL_FILE)

    def load_acl_file(self, file: str):
        if os.path.exists(os.path.join(self.basedir, file)):
            with open(os.path.join(self.basedir, file), 'r') as json_file:
                self.acl = json.load(json_file, cls=AclEntryDecoder)

    def save_acl_file(self, file: str = DEFAULT_ACL_FILE):
        with open(os.path.join(self.basedir, file), 'w') as json_file:
            json.dump(self.acl, json_file, cls=AclEntryEncoder)

    def make_folder(self, path):
        # Path is relative to top level directory
        # ACL must allow writing to the parent directory
        os.mkdir(os.path.join(self.basedir, path))

    def delete_folder(self, path):
        # Path is relative to top level directory
        # ACL must allow writing to the parent directory
        raise Exception("Not yet implemented")

    def delete_standard_file(self, path):
        # Path is relative to top level directory
        # ACL must allow writing to the parent directory and the existing file
        raise Exception("Not yet implemented")

    def save_standard_file(self, content, path):
        # Path is relative to top level directory
        # Will throw exception if path doesn't exist
        # Needs to check folder for write access, needs to check file exists and if so can it write to it?
        print("BASEDIR: "+self.basedir)
        print("PATH:" +os.path.join(self.basedir, path))
        f = open(os.path.join(self.basedir, path), 'w')
        f.write(content)
        f.close()

    def load_standard_file(self, path, user, roles):
        # Path is relative to top level directory
        # Will throw exception if file doesn't exist
        # Needs to check ACLs for read access
        if(path.startswith("/")):
            path = path[1:]
        if(os.path.exists(os.path.join(self.basedir,path))):
          f = open(os.path.join(self.basedir,path),'r')
          content = f.read()
          return content
        else:
            return None

    def get_repository_structure(self, file_filter):
        # Needs to scan repo
        # Should return everything except acl information
        # Should check ACLs to ensure user can see files or folders
        return clean_empty(path_to_dict(self.basedir, self.basedir, file_filter)['repoObjects'])

    def check(self, user: str, content: str, method: AclMethod):
        # Check for the owner and users permissions
        if self.acl.owner == ALL_OWNER or method in self.acl.users[user]:
            # By default, it the file is not listed it is allowed (blacklist behavior)
            if content not in self.acl.roles:
                return True
            # But if the the file is listed, we check its permissions
            if method in self.acl.roles[content]:
                return True
        return False

def clean_empty(d):
    if not isinstance(d, (dict, list)):
        return d
    if isinstance(d, list):
        return [v for v in (clean_empty(v) for v in d) if v]
    return {k: v for k, v in ((k, clean_empty(v)) for k, v in d.items()) if v or v == []}

def path_to_dict(path, basedir, file_filter):
    d = {'name': os.path.basename(path)}
    if os.path.isdir(path):
        d['type'] = "FOLDER"
        d['id'] = "#"+re.sub(basedir,"",path,count=1)
        d['path'] = re.sub(basedir,"",path,count=1)
        d['acl'] = ["READ", "WRITE", "GRANT"]
        d['repoObjects'] = [path_to_dict(os.path.join(path,x), basedir, file_filter) for x in os.listdir \
            (path)]
    else:
        extension = os.path.splitext(path)[1]
        if extension.startswith("."):
            extension = extension[1:]
        if (file_filter is not None and extension == file_filter) or file_filter is None:
            d['type'] = "FILE"
            d['name'] = os.path.basename(path)
            d['id'] = "#"+re.sub(basedir,"",path,count=1)
            d['path'] = re.sub(basedir,"",path,count=1)
            d['acl'] = ["READ", "WRITE", "GRANT"]
            d['fileType'] = 'saiku'
        else:
            return None
    return d



if __name__ == '__main__':
    fs = FilesystemContentRepository('/tmp/repo')
    fs.save_acl_file()
    o = path_to_dict("/tmp/repo", "/tmp/repo")
    print(json.dumps(o))

