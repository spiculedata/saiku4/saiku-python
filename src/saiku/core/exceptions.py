class SaikuException(Exception):
    status_code = 500
    message = 'An unexpected error has occurred.'


class SaikuUnauthorizedAccessException(SaikuException):
    status_code = 401
    message = 'Unauthorized request.'
