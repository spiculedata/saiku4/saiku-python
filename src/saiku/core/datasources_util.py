from saiku.rpc.rpc_client import SaikuRpcClient

import os
import json

DEFAULT_ENCODING = 'utf-8'


singleton_instance = None


class DatasourcesUtil(object):
    @staticmethod
    def init_instance(app):
        global singleton_instance
        if not singleton_instance:
            singleton_instance = DatasourcesUtil(app)
        return singleton_instance

    @staticmethod
    def get_instance():
        global singleton_instance
        return singleton_instance

    def __init__(self, app):
        self.app = app

    def get_datasource_folder(self):
        if not os.path.exists(os.path.join(self.app.config['UPLOAD_FOLDER'], 'datasources')):
            os.makedirs(os.path.join(self.app.config['UPLOAD_FOLDER'], 'datasources'))
        return os.path.join(self.app.config['UPLOAD_FOLDER'], 'datasources')

    def get_datasources(self):
        ds_folder = self.get_datasource_folder()

        if not os.path.exists(ds_folder):
            os.makedirs(ds_folder)

        return self.load_datasources()

    def save_datasource(self, data):
        datasource_file = open(os.path.join(self.get_datasource_folder(), data['connectionname']+".data"), 'w')
        json.dump(data, datasource_file)
        datasource_file.close()

    def load_datasources(self):
        directory = os.fsencode(self.get_datasource_folder())

        for file in os.listdir(directory):
            filename = os.fsdecode(file).decode('utf-8')
            if filename.endswith(".data"):
                datasource_data = None
                if os.path.exists(os.path.join(directory.decode(DEFAULT_ENCODING), filename)):
                    schemas_file = open(os.path.join(directory.decode(DEFAULT_ENCODING), filename), 'r')
                    datasource_data = json.load(schemas_file)
                    schemas_file.close()

                    schema = {}
                    schemas = self.get_schemas()
                    for s in schemas:
                        if s['name'] == datasource_data['schema']:
                            schema = s
                            break

                        schema_data = ''
                    if 'path' in schema and os.path.exists(schema['path']):
                        with open(schema['path']) as schema_file:
                            schema_data = schema_file.read()

                    datasource_data['schema_data'] = schema_data
                    saiku_rpc = SaikuRpcClient()
                    saiku_rpc.create_datasource(datasource_data)
            else:
                continue

    def get_schemas_folder(self):
        return os.path.join(self.app.config['UPLOAD_FOLDER'], 'schemas')

    def get_schemas_file(self):
        return os.path.join(self.get_schemas_folder(), 'schemas.json')

    def read_schema_json(self):
        with open(self.get_schemas_file()) as json_file:
            data = json.load(json_file)
            return data

    def create_schema(self, name, path):
        return {'name': name, 'path': path}

    def get_schemas(self):
        schemas_folder = self.get_schemas_folder()

        if not os.path.exists(schemas_folder):
            os.makedirs(schemas_folder)

        return self.load_schemas()

    def load_schemas(self):
        schemas = []
        if os.path.exists(self.get_schemas_file()):
            schemas_file = open(self.get_schemas_file(), 'r')
            schemas = json.load(schemas_file)
            schemas_file.close()
        return schemas

    def save_schemas(self, schemas):
        schemas_file = open(self.get_schemas_file(), 'w')
        json.dump(schemas, schemas_file)
        schemas_file.close()
