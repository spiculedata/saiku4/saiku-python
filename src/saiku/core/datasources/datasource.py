import xml
from xml.dom import minidom
from saiku.logging2 import get_logger


class RootMember(object):
    def __init__(self):
        self.uniqueName = ''
        self.name = ''
        self.caption = ''
        self.dimensionUniqueName = ''
        self.description = ''
        self.levelUniqueName = ''
        self.hierarchyUniqueName = ''
        self.calculated = False


class Level(object):
    def __init__(self):
        self.uniqueName = ''
        self.name = ''
        self.annotations = {}
        self.levelType = ''
        self.caption = ''
        self.hierarchyUniqueName = ''
        self.dimensionUniqueName = ''
        self.visible = True
        self.description = ''


class Hierarchy(object):
    def __init__(self):
        self.uniqueName = ''
        self.name = ''
        self.caption = ''
        self.dimensionUniqueName = ''
        self.levels = []
        self.visible = True
        self.description = ''
        self.rootMembers = []

    def add_level(self, level):
        self.levels.append(level)

    def add_root_member(self, root_member):
        self.rootMembers.append(root_member)


class Attribute(object):
    def __init__(self):
        self.name = ''
        self.hasHierarchy = ''
        self.keyColumn = ''
        self.hierarchyAllMemberName = ''
        self.keys = []
        self.names = []
        self.properties = []


class Dimension(object):
    def __init__(self):
        self.uniqueName = ''
        self.name = ''
        self.caption = ''
        self.description = ''
        self.visible = True
        self.hierarchies = []
        self.attributes = []
        self.table = ''
        self.key = ''

    def add_hierarchy(self, hierarchy):
        self.hierarchies.append(hierarchy)

    def add_attribute(self, attribute):
        self.attributes.append(attribute)


class CubeMetadata(object):
    def __init__(self):
        self.dimensions = []
        self.properties = {}

    def add_dimension(self, dimension):
        self.dimensions.append(dimension)

    def set_property(self, key: str, value: str):
        self.properties[key] = value

    def get_property(self, key: str) -> str:
        return self.properties[key]


class Cube(object):
    def __init__(self):
        self.uniqueName = ''
        self.name = ''
        self.connection = ''
        self.catalog = ''
        self.schema = ''
        self.caption = ''
        self.visible = True
        self.metadata = CubeMetadata()

    def set_metadata(self, metadata):
        self.metadata = metadata


class Column(object):
    def __init__(self):
        self.name = ''


class Key(object):
    def __init__(self):
        self.columns = []


class Name(object):
    def __init__(self):
        self.columns = []


class Property(object):
    def __init__(self):
        self.attribute = ''


class ForeignKey(object):
    def __init__(self):
        self.columns = []


class Table(object):
    def __init__(self):
        self.name = ''
        self.alias = ''
        self.keyColumn = ''
        self.keys = []
        self.foreignKeys = []
        self.columnDefs = []


class Link(object):
    def __init__(self):
        self.source = ''
        self.target = ''
        self.foreignKeys = []


class PhysicalSchema(object):
    def __init__(self):
        self.tables = []
        self.links = []


class Schema(object):
    def __init__(self):
        self.uniqueName = ''
        self.name = ''
        self.metamodelVersion = ''
        self.cubes = []
        self.physicalSchema = PhysicalSchema()

    def add_cube(self, cube):
        self.cubes.append(cube)


class Catalog(object):
    def __init__(self):
        self.uniqueName = ''
        self.name = ''
        self.schemas = []

    def add_schema(self, schema):
        self.schemas.append(schema)


class Datasource(object):
    def __init__(self):
        self.uniqueName = ''
        self.name = ''
        self.schema_file = ''
        self.driver = ''
        self.location = ''
        self.username = ''
        self.password = ''
        self.catalogs = []

    def add_catalog(self, catalog):
        self.catalogs.append(catalog)


class SchemaParser(object):
    _logger = get_logger('saiku.core.datasources.SchemaParser')

    @staticmethod
    def _get_val(el, key):
        if key in el.attributes:
            return el.attributes[key].value
        return None

    @staticmethod
    def _create_hierarchy(hierarchy_el):
        hierarchy = Hierarchy()
        hierarchy.name = SchemaParser._get_val(hierarchy_el, 'name')
        SchemaParser._logger.debug(f'\t\tAdding a hierarchy: {hierarchy.name}')

        for child in [element for element in hierarchy_el.childNodes if element.nodeType == xml.dom.Node.ELEMENT_NODE]:
            if child.tagName == 'Level':
                level = Level()

                if 'name' in child.attributes:
                    level.name = SchemaParser._get_val(child, 'name')
                    SchemaParser._logger.debug(f'\t\t\tAdding a level: {level.name}')
                    hierarchy.add_level(level)
                else:
                    pass  # Load a global level

        return hierarchy

    @staticmethod
    def _create_attribute(el):
        attribute = Attribute()

        attribute.name = SchemaParser._get_val(el, 'name')
        attribute.keyColumn = SchemaParser._get_val(el, 'keyColumn')
        attribute.hasHierarchy = SchemaParser._get_val(el, 'hasHierarchy')
        attribute.hierarchyAllMemberName = SchemaParser._get_val(el, 'hierarchyAllMemberName')

        for child in [element for element in el.childNodes if element.nodeType == xml.dom.Node.ELEMENT_NODE]:
            if child.tagName == 'Key':
                attribute.keys.append(SchemaParser._create_key(child))
            elif child.tagName == 'Name':
                attribute.names.append(SchemaParser._create_name(child))
            elif child.tagName == 'Property':
                prop = Property()
                prop.attribute = SchemaParser._get_val(child, 'attribute')
                attribute.properties.append(prop)

        return attribute

    @staticmethod
    def _create_dimension(dim_el):
        dim = Dimension()

        dim.name = SchemaParser._get_val(dim_el, 'name')
        dim.key = SchemaParser._get_val(dim_el, 'key')
        dim.table = SchemaParser._get_val(dim_el, 'table')

        SchemaParser._logger.debug(f'\tAdding a dimension: {dim.name}')

        for child in [element for element in dim_el.childNodes if element.nodeType == xml.dom.Node.ELEMENT_NODE]:
            if child.tagName == 'Hierarchies':
                for sub in [element for element in child.childNodes if element.nodeType == xml.dom.Node.ELEMENT_NODE]:
                    if sub.tagName == 'Hierarchy':
                        dim.add_hierarchy(SchemaParser._create_hierarchy(sub))
            elif child.tagName == 'Attributes':
                for sub in [element for element in child.childNodes if element.nodeType == xml.dom.Node.ELEMENT_NODE]:
                    if sub.tagName == 'Attribute':
                        dim.add_attribute(SchemaParser._create_attribute(sub))

        return dim

    @staticmethod
    def _create_cube(cube_el, global_dimensions):
        cube = Cube()
        cube.name = SchemaParser._get_val(cube_el, 'name')
        SchemaParser._logger.debug(f'Creating a cube: {cube.name}')

        for dim_el in cube_el.getElementsByTagName('Dimensions')[0].getElementsByTagName('Dimension'):
            if 'name' in dim_el.attributes:
                cube.metadata.add_dimension(SchemaParser._create_dimension(dim_el))
            elif 'source' in dim_el.attributes:
                source = SchemaParser._get_val(dim_el, 'source')
                if source in global_dimensions:
                    SchemaParser._logger.debug(f'\tAttaching the global dimension ({source}) to the cube')
                    cube.metadata.add_dimension(global_dimensions[source])

        return cube

    @staticmethod
    def _create_column(el):
        col = Column()

        col.name = SchemaParser._get_val(el, 'name')

        return col

    @staticmethod
    def _create_key(el):
        key = Key()

        for child in [element for element in el.childNodes if element.nodeType == xml.dom.Node.ELEMENT_NODE]:
            if child.tagName == 'Column':
                key.columns.append(SchemaParser._create_column(child))

        return key

    @staticmethod
    def _create_name(el):
        name = Name()

        for child in [element for element in el.childNodes if element.nodeType == xml.dom.Node.ELEMENT_NODE]:
            if child.tagName == 'Column':
                name.columns.append(SchemaParser._create_column(child))

        return name

    @staticmethod
    def _create_foreign_key(el):
        foreign_key = ForeignKey()

        for child in [element for element in el.childNodes if element.nodeType == xml.dom.Node.ELEMENT_NODE]:
            if child.tagName == 'Column':
                foreign_key.columns.append(SchemaParser._create_column(child))

        return foreign_key

    @staticmethod
    def _create_table(el):
        tbl = Table()

        tbl.name = SchemaParser._get_val(el, 'name')
        tbl.alias = SchemaParser._get_val(el, 'alias')
        tbl.keyColumn = SchemaParser._get_val(el, 'keyColumn')

        SchemaParser._logger.debug(f'\tCreating a table: {tbl.name}')

        for child in [element for element in el.childNodes if element.nodeType == xml.dom.Node.ELEMENT_NODE]:
            if child.tagName == 'Key':
                tbl.keys.append(SchemaParser._create_key(child))
            elif child.tagName == 'ForeignKey':
                tbl.keys.append(SchemaParser._create_foreign_key(child))

        return tbl

    @staticmethod
    def _create_link(el):
        link = Link()

        link.source = SchemaParser._get_val(el, 'source')
        link.target = SchemaParser._get_val(el, 'target')

        SchemaParser._logger.debug(f'\tCreating a link from {link.source} to {link.target}')

        return link

    @staticmethod
    def _create_physical_schema(el):
        ps = PhysicalSchema()

        for child in [element for element in el.childNodes if element.nodeType == xml.dom.Node.ELEMENT_NODE]:
            if child.tagName == 'Table':
                ps.tables.append(SchemaParser._create_table(child))
            elif child.tagName == 'Link':
                ps.links.append(SchemaParser._create_link(child))

        return ps

    @staticmethod
    def load_schema(file):
        schema = Schema()
        global_dimensions = {}

        doc = minidom.parse(file)
        schema_el = doc.childNodes[0]

        # Reading schema attributes
        schema.name = SchemaParser._get_val(schema_el, 'name')
        schema.metamodelVersion = SchemaParser._get_val(schema_el, 'metamodelVersion')

        # For every first level child nodes of the schema (root node)
        for child in [element for element in schema_el.childNodes if element.nodeType == xml.dom.Node.ELEMENT_NODE]:
            if child.tagName == 'PhysicalSchema':
                schema.physicalSchema = SchemaParser._create_physical_schema(child)
            elif child.tagName == 'Dimension':
                # Read the global dimensions
                global_dimensions[SchemaParser._get_val(child, 'name')] = SchemaParser._create_dimension(child)
            elif child.tagName == 'Cube':
                # Read the cubes
                schema.add_cube(SchemaParser._create_cube(child, global_dimensions))

        return schema


singleton_instance = None


class DatasourceManager(object):
    @staticmethod
    def get_instance():
        global singleton_instance
        if not singleton_instance:
            singleton_instance = DatasourceManager()
        return singleton_instance

    def __init__(self):
        self.datasources = []

    def add_datasource(self, datasource):
        self.datasources.append(datasource)

    def create_datasource(self, unique_name: str, name: str, schema, schema_file: str, driver: str, location: str,
                          username: str, password: str):
        datasource = Datasource()
        datasource.uniqueName = unique_name
        datasource.name = name
        datasource.schema = schema_file
        datasource.driver = driver
        datasource.location = location
        datasource.username = username
        datasource.password = password

        catalog = Catalog()
        catalog.uniqueName = unique_name
        catalog.name = name

        catalog.add_schema(schema)
        datasource.add_catalog(catalog)

        self.add_datasource(datasource)

    def get_datasources(self):
        return self.datasources


if __name__ == '__main__':
    SchemaParser.load_schema(open('../../../../data/FoodMart4.xml'))
