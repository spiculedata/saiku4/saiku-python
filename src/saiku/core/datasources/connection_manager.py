import os
import sqlite3

from saiku.config import config
from saiku.logging2 import get_logger
from saiku.core.datasources import SchemaParser, DatasourceManager

DEFAULT_MONDRIAN_DRIVER = 'mondrian.olap4j.MondrianOlap4jDriver'
DEFAULT_LOCATION = 'jdbc:mondrian:Jdbc=jdbc:mysql://localhost/test;JdbcDrivers=com.mysql.jdbc.Driver;'
DEFAULT_USERNAME = 'admin'
DEFAULT_PASSWORD = 'admin'

_logger = get_logger('saiku.core.datasources.ConnectionManager')

counter = 0


def _progress():
    """Utility function to display database loading progress messages.
    Loading the database files is sometimes a very slow process. This utility function just prints progress messages,
    so the user knows that something is happening under the hood.
    """
    global counter, _logger
    counter += 1
    _logger.debug('\t%d - Still loading data, please wait ...', counter)


class ConnectionManager(object):
    @staticmethod
    def load():
        ConnectionManager._load_earthquakes()
        ConnectionManager._load_foodmart()

    @staticmethod
    def _load_earthquakes():
        ConnectionManager._load_data(schema_file='Earthquakes.xml', ddl_file='earthquakes_ddl.sql',
                                     data_file='earthquakes_data.sql', db_name='earthquakes.db',
                                     log_prefix='Earthquakes')

    @staticmethod
    def _load_foodmart():
        ConnectionManager._load_data(schema_file='FoodMart4.xml', ddl_file='foodmart_ddl.sql',
                                     data_file='foodmart_data.sql', db_name='foodmart.db',
                                     log_prefix='FoodMart')

    @staticmethod
    def _load_data(schema_file: str, ddl_file: str, data_file: str, db_name: str, log_prefix: str):
        schema_file = os.path.join(config['DATA_DIR'], schema_file)
        db_name = os.path.join(config['DATA_DIR'], db_name)

        ConnectionManager._load_schema(schema_file, log_prefix)

        if not os.path.exists(db_name):
            # We won't be loading the data from database at the Python side, because it's done at the Java side
            #ConnectionManager._load_sql_ddl(os.path.join(config['DATA_DIR'], ddl_file), db_name, log_prefix)
            #ConnectionManager._load_sql_data(os.path.join(config['DATA_DIR'], data_file), db_name, log_prefix)
            pass
        else:
            _logger.debug('(%s) - The database %s already exists', log_prefix, db_name)

    @staticmethod
    def _load_schema(schema_file: str, log_prefix: str,
                     driver: str = DEFAULT_MONDRIAN_DRIVER,
                     location: str = DEFAULT_LOCATION,
                     username: str = DEFAULT_USERNAME,
                     password: str = DEFAULT_PASSWORD):
        schema = SchemaParser.load_schema(schema_file)
        dm = DatasourceManager.get_instance()
        with open(schema_file, 'r') as file:
            dm.create_datasource(log_prefix, log_prefix, schema, file.read(), driver, location, username, password)

    @staticmethod
    def _load_sql_ddl(sql_file: str, db_name: str, log_prefix: str):
        global counter

        _logger.debug('(%s) - Creating the %s database', log_prefix, db_name)
        conn = sqlite3.connect(db_name)

        _logger.debug('(%s) - Loading %s file', log_prefix, sql_file)
        f = open(sql_file, encoding=config['DEFAULT_ENCODING'])
        sql = f.read()
        _logger.debug('(%s) - Done loading %s file', log_prefix, sql_file)

        counter = 0
        conn.set_progress_handler(_progress, 10)
        cur = conn.cursor()

        _logger.debug('(%s) - Executing the DDL script', log_prefix)
        try:
            cur.executescript(sql)
        except Exception as e:
            _logger.exception('(%s) - Could not load the %s database: %s', log_prefix, db_name, e)
        finally:
            conn.commit()
            conn.close()

        _logger.debug('(%s) - Done creating the database %s', log_prefix, db_name)

    @staticmethod
    def _load_sql_data(sql_file: str, db_name: str, log_prefix: str):
        _logger.debug('(%s) - Loading data into the %s database', log_prefix, db_name)
        conn = sqlite3.connect(db_name)
        cur = conn.cursor()

        _logger.debug('(%s) - Loading %s file', log_prefix, sql_file)
        with open(sql_file, encoding=config['DEFAULT_ENCODING']) as f:
            lines = f.readlines()
            for i, line in enumerate(lines):
                try:
                    _logger.debug('\t(%s) - Inserting data %d of %d', log_prefix, i, len(lines))
                    cur.execute(line)
                except Exception as e:
                    _logger.exception('(%s) - Error (%s) while trying to execute the statement: %s, at line %d',
                                      log_prefix, e, db_name, i)

        conn.commit()
        conn.close()
        _logger.debug('(%s) - Done loading data into the %s database', log_prefix, db_name)
