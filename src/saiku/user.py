from string import Template

DEFAULT_USER_LANGUAGE = 'en'
DEFAULT_USER_ROLES = ['ROLE_USER']


class User(object):
    def __init__(self, user_id: int, username: str, email: str, password: str, is_admin=False, roles=DEFAULT_USER_ROLES,
                 language=DEFAULT_USER_LANGUAGE):
        self.user_id = user_id
        self.username = username
        self.email = email
        self.password = password
        self.is_admin = is_admin
        self.roles = roles
        self.language = language

    def to_dict(self):
        return {"username": self.username, "email": self.email, "password": "hidden", "roles": self.roles,
                "id": self.user_id}

    def __str__(self):
        return "User(id='%s')" % self.id
