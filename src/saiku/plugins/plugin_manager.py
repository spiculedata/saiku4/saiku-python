def get_available_plugins():
    return [{"name":"AdminConsole","description":"","path":"js/saiku/plugins/AdminConsole/plugin.js"},
            {"name":"BIServer","description":"","path":"js/saiku/plugins/BIServer/plugin.js"},
            {"name":"CCC_Chart","description":"","path":"js/saiku/plugins/CCC_Chart/plugin.js"},
            {"name":"ChangeLocale","description":"","path":"js/saiku/plugins/ChangeLocale/plugin.js"},
            {"name":"Fullscreen","description":"","path":"js/saiku/plugins/Fullscreen/plugin.js"},
            {"name":"I18n","description":"","path":"js/saiku/plugins/I18n/plugin.js"},
            {"name":"Intro","description":"","path":"js/saiku/plugins/Intro/plugin.js"},
            {"name":"Statistics","description":"","path":"js/saiku/plugins/Statistics/plugin.js"}]
