import binascii
import hashlib
import os

from saiku.user import User
from saiku.config import config

import json

# Sample in memory users database
users = [
    User(1, 'admin', 'admin@email.com', '856baf65415d8ab3260d811e3361c934cca3a2c8350af29322ac2dfd1e6b3c51e94f00234498a1e3ed07d03d56ef00e3e124319d9866ceb16faee2927c58c178fd1ca373eb59869c8dda7be129ab02f3549dad7c8f81e79a7e4e1e0de8d3afc4', is_admin=True, roles=["ROLE_ADMIN", "ROLE_USER"]),
    User(2, 'demo', 'demo@email.com', '856baf65415d8ab3260d811e3361c934cca3a2c8350af29322ac2dfd1e6b3c5145f439c7a5fce5efba4e765c0535196779105aa9309d725f681aa9fe117a16b22e4d2932d52cd79858a83202b2e9c03c6753635ea534a4a5f2e3c8e9647eaebd')
]


def get_next_user_id():
    max_user_id = 0
    for u in users:
        if u.user_id > max_user_id:
            max_user_id = u.user_id
    return max_user_id + 1


class UserJSONEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, User):
            return obj.__dict__
        else:
            return json.JSONEncoder.default(self, obj)


class UserJSONDecoder(json.JSONDecoder):
    def __init__(self, *args, **kwargs):
        json.JSONDecoder.__init__(self, object_hook=UserJSONDecoder.object_hook, *args, **kwargs)

    @staticmethod
    def object_hook(dct):
        user_id = dct['user_id']
        username = dct['username']
        email = dct['email']
        password = dct['password']
        is_admin = dct['is_admin']
        roles = dct['roles']
        language = dct['language']
        return User(user_id, username, email, password, is_admin, roles, language)


class UserDatabase(object):
    def __init__(self):
        self._username_table = []
        self._user_id_table = []

        file = self._get_user_file()

        if os.path.exists(file):
            UserDatabase._read_users_file()
        else:
            print("Cant find user file, using demo users")

        self._update_lookup_tables()

    def _update_lookup_tables(self):
        self._username_table = {u.username: u for u in users}
        self._user_id_table = {u.user_id: u for u in users}

    def find_user_by_username(self, username):
        return self._username_table.get(username, None)

    def find_user_by_user_id(self, user_id):
        return self._user_id_table.get(user_id, None)

    def to_json(self):
        global users
        return json.dumps([u.to_dict() for u in users])

    @staticmethod
    def _get_user_file():
        if not os.path.exists(os.path.join(config['UPLOAD_FOLDER'], 'users')):
            os.makedirs(os.path.join(config['UPLOAD_FOLDER'], 'users'))

        return os.path.join(config['UPLOAD_FOLDER'], 'users', 'users.json')

    @staticmethod
    def hash_password(password):
        """Hash a password for storing."""
        salt = hashlib.sha256(os.urandom(60)).hexdigest().encode('ascii')
        pwdhash = hashlib.pbkdf2_hmac('sha512', password.encode('utf-8'),
                                  salt, 100000)
        pwdhash = binascii.hexlify(pwdhash)
        return (salt + pwdhash).decode('ascii')

    @staticmethod
    def verify_password(stored_password, provided_password):
        """Verify a stored password against one provided by user"""
        salt = stored_password[:64]
        stored_password = stored_password[64:]
        pwdhash = hashlib.pbkdf2_hmac('sha512',
                                  provided_password.encode('utf-8'),
                                  salt.encode('ascii'),
                                  100000)
        pwdhash = binascii.hexlify(pwdhash).decode('ascii')
        return pwdhash == stored_password

    @staticmethod
    def _save_users_file():
        with open(UserDatabase._get_user_file(), 'w') as users_file:
            json.dump(users, users_file, cls=UserJSONEncoder)

    @staticmethod
    def _read_users_file():
        global users
        with open(UserDatabase._get_user_file(), 'r') as users_file:
            users = json.load(users_file, cls=UserJSONDecoder)

    def add_user(self, user):
        if self.find_user_by_username(user.username):
            self.update_user(user)
        else:
            user.user_id = get_next_user_id()
            user.password = UserDatabase.hash_password(user.password)
            users.append(user)
            UserDatabase._save_users_file()
            self._update_lookup_tables()

    def update_user(self, user):
        self.delete_user(user.user_id)
        user.password = UserDatabase.hash_password(user.password)
        self.add_user(user)

    def delete_user(self, user_id):
        idx = -1
        for i, u in enumerate(users):
            if u.user_id == user_id:
                idx = i
                break
        if idx >= 0:
            users.pop(idx)
            UserDatabase._save_users_file()
            self._update_lookup_tables()


# Just for testing
if __name__ == '__main__':
    ud = UserDatabase()
    print(ud.to_json())
    print('user file', UserDatabase._get_user_file())
    print('Saving users file ...')
    UserDatabase._save_users_file()
    print('Loading users file ...')
    UserDatabase._read_users_file()
    print(ud.to_json())
