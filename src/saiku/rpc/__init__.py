from flask_cors import CORS
from flask import Flask
import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration

sentry_sdk.init(
    dsn="https://9fe3a4a77e314659933c5d08039802af@sentry.meteorite.bi/5",
    integrations=[FlaskIntegration()]
)


# Create the Flask APP
application = Flask(__name__)
application.debug = True

CORS(application)  # Enables the Cross Origin Resource sharing for the application
