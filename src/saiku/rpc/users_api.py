from flask import Blueprint, request
from flask_jwt_extended import jwt_required
from saiku.rpc.util import make_json_response
from saiku.licensing import license_manager
from saiku.user import User
from saiku.database import UserDatabase
import json


users_api = Blueprint('users_api', __name__)


def build_user_obj_from_request(request_data):
    user = User(None, request_data['username'], request_data['email'], request_data['password'], False,
                request_data['roles'])
    for r in user.roles:
        if r == 'ROLE_ADMIN':
            user.is_admin = True
    return user


@jwt_required
@users_api.route("/saiku/rest/saiku/admin/users", methods=["POST"])
def add_user():
    user = build_user_obj_from_request(request.json)
    UserDatabase().add_user(user)
    return make_json_response('{"status": "ok"}')


@jwt_required
@users_api.route("/saiku/rest/saiku/<username>/users", methods=['GET'])
def get_users(username):
    """
        Get users
        ---
        tags:
          - users
        responses:
          200:
            description:
        """
    return make_json_response(license_manager.get_users())


@jwt_required
@users_api.route("/saiku/rest/saiku/admin/users/<user_id>", methods=["GET"])
def get_user(user_id):
    user = UserDatabase().find_user_by_user_id(int(user_id))
    return make_json_response(json.dumps(user.to_dict()))


@jwt_required
@users_api.route("/saiku/rest/saiku/admin/users/<user_id>", methods=["PUT"])
def update_user(user_id):
    user = build_user_obj_from_request(request.json)
    user.user_id = int(user_id)
    UserDatabase().update_user(user)
    return make_json_response('{"status": "ok"}')


@jwt_required
@users_api.route("/saiku/rest/saiku/admin/users/<user_id>", methods=["DELETE"])
def delete_user(user_id):
    UserDatabase().delete_user(int(user_id))
    return make_json_response('{"status": "ok"}')
