from flask import Blueprint, request, make_response
from flask_jwt_extended import jwt_required
from saiku.rpc.rpc_client import SaikuRpcClient
from saiku.metrics.metrics import Metrics

import json
import os

query_api = Blueprint('query_api', __name__)
metrics = Metrics.get_instance()


# The following methods are related to query execution
@query_api.route("/saiku/rest/saiku/api/query/execute", methods=['POST'])
@jwt_required
def execute_query():
    """
        Execute the open query
        ---
        tags:
          - query
        responses:
          200:
            description: Query Resultset
        """
    if os.environ.get('INIT') == "true":
        metrics.post_point(metrics.query(request.headers.get('User-Agent')))

    saiku_rpc = SaikuRpcClient()
    query_data = json.dumps(request.json)
    return make_response(saiku_rpc.execute_query(query_data))
