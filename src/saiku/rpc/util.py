from flask import make_response


def make_json_response(data, http_code=200):
    response = make_response(data, http_code)
    response.headers.set('Content-Type', 'application/json')
    return response
