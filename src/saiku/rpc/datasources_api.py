from flask import Blueprint, request, make_response
from flask_jwt_extended import jwt_required
from saiku.rpc.rpc_client import SaikuRpcClient
from saiku.core.datasources_util import DatasourcesUtil
from saiku.metrics.metrics import Metrics

import json
import os

ds_api = Blueprint('datasources_api', __name__)
metrics = Metrics.get_instance()


@ds_api.route("/saiku/rest/saiku/<username>/datasources", methods=['GET'])
@jwt_required
def get_datasources(username):
    """
        Get Data Sources
        ---
        tags:
          - datasources
        responses:
          200:
            description: Data Source Object
        """
    saiku_rpc = SaikuRpcClient()
    return make_response(saiku_rpc.get_datasources(username))


@ds_api.route("/saiku/rest/saiku/<username>/datasources", methods=['POST'])
@jwt_required
def create_datasource(username):
    """
        Create New Data Source
        ---
        tags:
          - datasources
        responses:
          200:
            description: Data Source Object
        """
    saiku_rpc = SaikuRpcClient()

    datasource_data = request.json

    if os.environ.get('INIT'):
        metrics.post_point(metrics.query(datasource_data['driver']))

    schema = {}
    schemas = DatasourcesUtil.get_instance().get_schemas()
    for s in schemas:
        if s['name'] == datasource_data['schema']:
            schema = s
            break

    schema_data = ''
    if 'path' in schema and os.path.exists(schema['path']):
        with open(schema['path']) as schema_file:
            schema_data = schema_file.read()
    DatasourcesUtil.get_instance().save_datasource(datasource_data)
    datasource_data['schema_data'] = schema_data
    return make_response(saiku_rpc.create_datasource(datasource_data))


@ds_api.route("/saiku/rest/saiku/<username>/datasources/<datasource_id>", methods=['PUT'])
@jwt_required
def edit_datasource(username, datasource_id):
    """
        Edit Data Source
        ---
        tags:
          - datasources
        responses:
          200:
            description: Data Source Object
        """
    saiku_rpc = SaikuRpcClient()
    datasource_data = json.dumps(request.json)
    return make_response(saiku_rpc.edit_datasource(username, datasource_id, datasource_data))


@ds_api.route("/saiku/rest/saiku/<username>/datasources/<datasource_id>", methods=['DELETE'])
@jwt_required
def delete_datasource(username, datasource_id):
    """
        Delete Data Source
        ---
        tags:
          - datasources
        responses:
          200:
            description: Ok
        """
    saiku_rpc = SaikuRpcClient()
    return make_response(saiku_rpc.delete_datasource(username, datasource_id))
