from flask import Blueprint, request, make_response, send_from_directory
from flask_jwt_extended import jwt_required
from saiku.core.datasources_util import DatasourcesUtil
from saiku.metrics.metrics import Metrics
from werkzeug.utils import secure_filename

import json
import os

schemas_api = Blueprint('schemas_api', __name__)
metrics = Metrics.get_instance()


@schemas_api.route("/saiku/rest/saiku/<username>/schema/<name>")
@jwt_required
def download_schema(username, name):
    return send_from_directory(directory=DatasourcesUtil.get_instance().get_schemas_folder(), filename=name + ".xml")


@schemas_api.route("/saiku/rest/saiku/<username>/schema", methods=['GET'])
@jwt_required
def get_schemas(username):
    """
        Get Available Schema
        ---
        tags:
          - datasources
        responses:
          200:
            description: Ok
        """
    return json.dumps(DatasourcesUtil.get_instance().get_schemas())


@schemas_api.route("/saiku/rest/saiku/<username>/schema/<schema_id>", methods=['PUT', 'POST'])
@jwt_required
def add_schema(username, schema_id):
    """
        Add New Schema
        ---
        tags:
          - datasources
        responses:
          200:
            description: Ok
        """
    schemas = DatasourcesUtil.get_instance().get_schemas()
    file = request.files['file']
    filename = secure_filename(file.filename)
    schemas_folder = DatasourcesUtil.get_instance().get_schemas_folder()

    file.save(os.path.join(schemas_folder, schema_id + ".xml"))
    schemas.append(
        DatasourcesUtil.get_instance().create_schema(schema_id, os.path.join(schemas_folder, schema_id + ".xml")))
    DatasourcesUtil.get_instance().save_schemas(schemas)

    return json.dumps(schemas)


@schemas_api.route("/saiku/rest/saiku/admin/schema/<name>", methods=['DELETE'])
def delete_schema(name):
    os.remove(os.path.join(DatasourcesUtil.get_instance().get_schemas_folder(), name + ".xml"))
    data = DatasourcesUtil.get_instance().read_schema_json()
    data = [x for x in data if x['name'] not in [name]]
    DatasourcesUtil.get_instance().save_schemas(data)
    return make_response(json.dumps({"message": "schema deleted"}))
