import pika
import uuid
import json
from saiku.core.ee.license import create_trial_license, create_full_license, fetch_license


class SaikuRpcClient(object):
    def __init__(self):
        self.connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
        self.channel = self.connection.channel()
        self.callback_queue = self.channel.queue_declare('', exclusive=True).method.queue
        self.channel.basic_consume('', self.on_response, auto_ack=True)
        self.response = None
        self.corr_id = None

    def on_response(self, ch, method, props, body):
        if self.corr_id == props.correlation_id:
            self.response = body

    def call(self, message, body='', license_key='', company='', name=''):
        self.response = None
        self.corr_id = str(uuid.uuid4())
        l = fetch_license()
        license_key = l['key']
        company = l['company']
        name = l['name']

        self.channel.basic_publish(exchange='',
                                   routing_key=message,
                                   properties=pika.BasicProperties(
                                       reply_to=self.callback_queue,
                                       correlation_id=self.corr_id,
                                   ),
                                   body=SaikuRpcClient.create_saiku_request(license_key, name, company, body))
        while self.response is None:
            self.connection.process_data_events()
        return self.response

    def health_check(self):
        print("Checking health")
        return self.connection.is_open


    @staticmethod
    def create_saiku_request(license_key, name, company, payload):
        return json.dumps({'licenseKey': license_key, 'name': name, 'company': company, 'jsonPayload': payload})

    # Business methods section
    def email_password_auth(self, email, password):
        data = {
            'email': email,
            'password': password
        }
        return self.call('email_password_auth', json.dumps(data))

    def cookie_auth(self, cookie_value):
        data = {'cookie_value': cookie_value}
        return self.call('cookie_auth', json.dumps(data))

    def cas_header_auth(self, cas_header):
        data = {'cas_header': cas_header}
        return self.call('cas_header_auth', json.dumps(data))

    def generate_report(self):
        return self.call('generate_report')

    def create_datasource(self, datasource_data):
        return self.call('create_datasource', json.dumps(datasource_data))

    def edit_datasource(self, username, datasource_id, datasource_data):
        data = {'username': username, 'datasource_id': datasource_id, 'datasource': datasource_data}
        return self.call('edit_datasource', json.dumps(data))

    def delete_datasource(self, username, datasource_id):
        data = {'username': username, 'datasource_id': datasource_id}
        return self.call('delete_datasource', json.dumps(data))

    def get_datasources(self, username):
        data = {'username': username}
        return self.call('list_datasources', json.dumps(data))

    def discover_datasources(self, username):
        data = {'username': username}
        return self.call('discover_datasources', json.dumps(data))

    def get_cube_metadata(self, username, connection, catalog, schema, cube):
        data = {'username': username, 'connection': connection, 'catalog': catalog, 'schema': schema, 'cube': cube}
        return self.call('get_cube_metadata', json.dumps(data))

    def execute_query(self, query_json):
        return self.call('execute_query', query_json)

    def validate_license(self, lic):
        return self.call('validate_license_key', json.dumps(lic))
