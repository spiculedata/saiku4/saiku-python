from flask import Blueprint, request, make_response, jsonify, g, request, Response

from saiku.core.repositories.fs_content_repository import FilesystemContentRepository
from saiku.rpc import application as app
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,
    get_jwt_identity
)
import os
import json

from saiku.rpc.rpc_client import SaikuRpcClient

repo_api = Blueprint('repo_api', __name__)


fs = FilesystemContentRepository('/repo')


@repo_api.route("/saiku/rest/saiku/api/repository", methods=["GET"])
@jwt_required
def get_files():
    file_filter = request.args.get('type')
    #print("FILTER TYPE: "+file_filter)
    return json.dumps(fs.get_repository_structure(file_filter))


@repo_api.route("/saiku/rest/saiku/api/repository/resource", methods=["POST"])
@jwt_required
def save_file():
    file = request.form['file']
    if file.startswith('/'):
        file = file[1:]

    if 'content' in request.form:
        content = request.form['content']
        fs.save_standard_file(content, file)
    else:
        fs.make_folder(file)
    return make_response("hello")

@repo_api.route("/saiku/rest/saiku/api/repository/resource", methods=["GET"])
@jwt_required
def load_file():
    data = fs.load_standard_file(request.args.get("file"), None, None)

    response = Response(data, mimetype="text/plain")
    response.headers.add('content-length', str(len(data)))
    return response

@app.route("/saiku/rest/saiku/api/query/<path:path>", methods=['POST'])
@jwt_required
def create_query(path):
    """
        Create a new query
        ---
        tags:
          - query
        responses:
          200:
            description: Ok message
        """
    f = None
    if('file' in request.form):
        f = fs.load_standard_file(request.form['file'], None, None)
        saiku_rpc = SaikuRpcClient()
    elif('json' in request.form):
        f = request.form['json']
        saiku_rpc = SaikuRpcClient()



    return make_response(saiku_rpc.execute_query(f))


@app.route("/saiku/rest/saiku/embed/export/saiku/json")
@jwt_required
def export_json():
    file = request.args.get('file')
    formatter = request.args.get('formatter')
    #print("FILE:"+file)
    f = fs.load_standard_file(file, None, None)
    saiku_rpc = SaikuRpcClient()

    return make_response(saiku_rpc.execute_query(f))



def _save_file(content, name):
    schemas_file = open(_get_repo_folder()+"/"+name, 'w')
    json.dump(content, schemas_file)
    schemas_file.close()


def _get_repo_folder():
    return os.path.join(app.config['UPLOAD_FOLDER'], 'repo')
