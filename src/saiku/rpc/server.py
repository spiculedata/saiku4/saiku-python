from flask import make_response, jsonify, request
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,
    get_jwt_identity
)
from flask_swagger import swagger

from saiku.rpc.rpc_client import SaikuRpcClient
from saiku.rpc.util import make_json_response
from saiku.security import auth_manager
from saiku.session import saiku_session
from saiku.licensing import license_manager
from saiku.plugins import plugin_manager
from saiku.rpc import application as app
from saiku.config import config
from saiku.metrics.metrics import Metrics
from saiku.core.ee.license import create_full_license, fetch_license
from saiku.core.datasources_util import DatasourcesUtil
from saiku.core.exceptions import (SaikuException, SaikuUnauthorizedAccessException)
from saiku.rpc.repo_api import repo_api
from saiku.rpc.users_api import users_api
from saiku.rpc.query_api import query_api
from saiku.rpc.datasources_api import ds_api
from saiku.rpc.schemas_api import schemas_api

import os
import json
import datetime
import time

app.config['JWT_SECRET_KEY'] = config['JWT_SECRET']
jwt = JWTManager(app)

"""
Upload configuration
"""

app.config['UPLOAD_FOLDER'] = config['UPLOAD_FOLDER']
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024

# Register other routes (blueprints)
app.register_blueprint(repo_api)
app.register_blueprint(users_api)
app.register_blueprint(query_api)
app.register_blueprint(ds_api)
app.register_blueprint(schemas_api)


metrics = Metrics.get_instance()


@app.before_first_request
def bootstrap_metrics():
    global metrics
    if os.environ.get('INIT') == "true":
        metrics.startup()

    saiku_rpc = SaikuRpcClient()
    loop = True
    DatasourcesUtil.init_instance(app)

    while loop:
        if saiku_rpc.health_check():
            print("Rabbit MQ alive loading data sources")
            DatasourcesUtil.get_instance().load_datasources()
            lic = fetch_license()

            saiku_rpc = SaikuRpcClient()
            resp = saiku_rpc.validate_license(lic)
            loop = False
        else:
            print("waiting for rabbitmq...")
            time.sleep(5)


"""
Public Methods Section
Here are the methods that handle HTTP requests
"""
@app.route("/")
def index():
    """
        Test API Operation
        ---
        tags:
          - api
        responses:
          200:
            description: Hello world
        """
    response = make_response('<h1>Saiku 4 - API Server</h1>')
    response.headers.set('Content-Type', 'text/html')

    return response


@app.route('/saiku/rest/saiku/session', methods=['POST'])
def create_session():
    """
        Create Session
        ---
        tags:
          - session
        responses:
          200:
            description: Session Object
    """
    metrics.post_point(metrics.session(request.headers.get('User-Agent')))
    return _create_session()


@app.route('/saiku/rest/saiku/session', methods=['GET'])
@jwt_required
def fetch_session():
    """
        Fetch Session
        ---
        tags:
          - session
        responses:
          200:
            description: Session Object
        """
    return _fetch_session(get_jwt_identity())


@app.route('/saiku/rest/saiku/api/license/quota')
@jwt_required
def quota():
    if _is_logged_in():
        return make_json_response(json.dumps({"junk":600}))
    else:
        raise SaikuUnauthorizedAccessException()


@app.route('/saiku/rest/saiku/session', methods=['DELETE'])
@jwt_required
def delete_session():
    """
        Delete Session
        ---
        tags:
          - session
        responses:
          200:
            description: Ok
        """
    saiku_session.delete_session(get_jwt_identity())
    return make_json_response('{"status": "ok"}')


# License endpoints
@app.route("/saiku/rest/saiku/api/license/users", methods=['GET'])
@jwt_required
def get_license_users():
    """
        Get Users associated with the license
        ---
        tags:
          - license
        responses:
          200:
              description: A User object.
              schema:
                type: object
                properties:
                  username:
                    type: string
                    example: admin
                  email:
                    type: string
                    example: info@meteorite.bi
                  password:
                    type: string
                    example: hidden
                  roles:
                    type: list
                    example: ["ROLE_ADMIN","ROLE_USER"]
                  id:
                    type: integer
                    example: 1

        """
    return license_manager.get_users()


@app.route('/saiku/rest/saiku/api/license', methods=['GET'])
@jwt_required
def get_license():
    """
        Get License
        ---
        tags:
          - license
        responses:
          200:
            description: License Object
            schema:
                type: object
                properties:
                    email:
                      type: string
                      example: info@meteorite.bi
                    expiration:
                      type: integer
                      example: 100000
                    licenseNumber:
                      type: string
                      example: X233DL-CSKDDK-43KFDF32
                    licenseType:
                      type: string
                      example: Saiku 4 Enterprise Edition - Trial
                    name:
                      type: string
                      example: Tom Barber
                    userLimit:
                      type: string
                      example: 1000

        """
    if _is_logged_in():
        l = fetch_license()

        saiku_rpc = SaikuRpcClient()
        resp = saiku_rpc.validate_license(l)
        my_json = resp.decode('utf8').replace("'", '"')
        resp = json.loads(my_json)
        j = {
            "company": resp['company'],
            "expiration": resp['expiration'],
            "licenseNumber": resp['key'],
            "licenseType": resp['type'],
            "name": resp['name'],
            "userLimit": resp['numberOfUsers']
        }
        return make_json_response(json.dumps(j))
    else:
        raise SaikuUnauthorizedAccessException()


@app.route('/saiku/rest/saiku/api/license', methods=['POST'])
def set_license_key():
    """
        Set License Key
        ---
        tags:
          - license
        requestBody:
          description: License Request
          required: true
          content:
            application/json:
              schema:
                type: object
        responses:
          200:
            description: Ok
        """
    license_data = request.form['key']
    license_name = request.form['name']
    license_company = request.form['company']
    #if 'license_type' in license_data and license_data['license_type'] == 'full':
    #    create_full_license()
    #else:
    #    create_trial_license()
    create_full_license(license_data, license_name, license_company)
    l = fetch_license()

    saiku_rpc = SaikuRpcClient()
    resp = saiku_rpc.validate_license(l)
    #print(resp)
    return make_json_response(json.dumps({'status': 'ok', 'message': 'License created successfully'}))


# Saiku general health endpoint
@app.route('/saiku/rest/saiku/info')
def get_available_plugins():
    """
        Get Available Plugins
        ---
        tags:
          - plugins
        responses:
          200:
            description: JSON Schema
            schema:
                type: array
                items:
                  type: object
                  properties:
                      name:
                          type: string
                          example: Admin Console
                      description:
                          type: string
                          example: The admin console plugin
                      path:
                          type: string
                          example: js/saiku/plugins/AdminConsole/plugin.js
        """
    data = plugin_manager.get_available_plugins()
    return make_json_response(json.dumps(data))


# Datasources and Cubes endpoints
@app.route('/saiku/rest/saiku/<username>/discover')
@jwt_required
def discover(username):
    """
        Discover available data objects
        ---
        tags:
          - discover
        responses:
          200:
            description: Discovery Object
            schema:
                type: array
                items:
                    type: object
                    properties:
                        uniqueName:
                            type: string
                            example: foodmart
                        catalogs:
                            type: array
                            items:
                              type: object
                              properties:
                                  uniqueName:
                                      type: string
                                      example: FoodMart
                                  name:
                                      type: string
                                      example: FoodMart
                                  schemas:
                                      type: array
                                      items:
                                          type: object
                                          properties:
                                              uniqueName:
                                                  type: string
                                                  example: FoodMart
                                              name:
                                                  type: string
                                                  example: FoodMart
                                              cubes:
                                                  type: array
                                                  items:
                                                      type: object
                                                      properties:
                                                          uniqueName:
                                                              type: string
                                                              example: [foodmart].[FoodMart].[FoodMart].[HR]
                                                          name:
                                                              type: string
                                                              example: FoodMart
                                                          connection:
                                                              type: string
                                                              example: foodmart
                                                          catalog:
                                                              type: string
                                                              example: FoodMart
                                                          schema:
                                                              type: string
                                                              example: FoodMart
                                                          caption:
                                                              type: string
                                                              example: HR
                                                          visible:
                                                              type: boolean
                                                              example: true
        """
    if _is_logged_in():
        saiku_rpc = SaikuRpcClient()
        response = saiku_rpc.discover_datasources(username)
        return make_json_response(response)
    else:
        raise SaikuUnauthorizedAccessException()


@app.route('/saiku/rest/saiku/<username>/discover/refresh')
@jwt_required
def discover_refresh(username):
    """
        Refresh available data objects
        ---
        tags:
          - discover
        responses:
          200:
            description: Discovery Object
        """
    if _is_logged_in():
        saiku_rpc = SaikuRpcClient()
        response = saiku_rpc.discover_datasources(username)
        #print(response.decode('utf-8'))
        if "stackTrace" in response.decode('utf-8'):
            #print(response.decode('utf-8'))
            return make_json_response(response, 500)
        return make_json_response(response)
    else:
        raise SaikuUnauthorizedAccessException()


@app.route('/saiku/rest/saiku/<username>/discover/<connection>/<catalog>/<schema>/<cube>/metadata')
@jwt_required
def discover_metadata(username, connection, catalog, schema, cube):
    """
        Discover Metadata
        ---
        tags:
          - discover
        responses:
          200:
            description: Metadata Object
        """
    if _is_logged_in():
        saiku_rpc = SaikuRpcClient()
        response = saiku_rpc.get_cube_metadata(username, connection, catalog, schema, cube)
        return make_json_response(response)
    else:
        raise SaikuUnauthorizedAccessException()


# Other authentication methods
@app.route("/email_password_auth")
def email_password_auth():
    """
        Email Password Endpoint
        ---
        tags:
          - authentication
        responses:
          200:
            description: Ok
        """
    saiku_rpc = SaikuRpcClient()
    return make_response(saiku_rpc.email_password_auth('admin', 'admin'))


@app.route("/cookie_auth")
def cookie_auth():
    """
        Cookie Authentication Endpoint
        ---
        tags:
          - authentication
        responses:
          200:
            description: Ok
        """
    saiku_rpc = SaikuRpcClient()
    return make_response(saiku_rpc.cookie_auth('admin'))


@app.route("/cas_header_auth")
def cas_header_auth():
    """
        CAS Header Authentication
        ---
        tags:
          - authentication
        responses:
          200:
            description: Ok
        """
    saiku_rpc = SaikuRpcClient()
    return make_response(saiku_rpc.cas_header_auth('admin'))


@app.route("/report")
def report():
    """
        Report Export Endpoint
        ---
        tags:
          - query
        responses:
          200:
            description: Ok
        """
    saiku_rpc = SaikuRpcClient()
    response = make_response(saiku_rpc.generate_report())
    response.headers.set('Content-Disposition', 'inline', filename='saiku_report.pdf')
    response.headers.set('Content-Type', 'application/pdf')
    return response


@app.route("/saiku/rest/saiku/admin/datakeys", methods=['GET'])
@jwt_required
def get_datakeys():
    """
        Get Data Keys
        ---
        tags:
          - keys
        responses:
          200:
            description: Ok
        """
    return '[]'


@app.route("/spec")
def spec():
    swag = swagger(app)
    swag['info']['version'] = "4.0"
    swag['info']['title'] = "Saiku Server"
    return jsonify(swag)


# Error handler implementation
@app.errorhandler(SaikuException)
def handle_errors(e):
    error_data = {
        'success': False,
        'error': {
            'type': str(type(e)),
            'message': e.message
        }
    }

    response = make_response(json.dumps(error_data), e.status_code)
    response.headers.set('Content-Type', 'application/json')

    return response


# Protected methods
def _create_session():
    username, password = request.form['username'], request.form['password']

    if not username:
        return jsonify({"msg": "Missing username parameter"}), 400
    if not password:
        return jsonify({"msg": "Missing password parameter"}), 400

    user = auth_manager.authenticate(username, password)

    if not user:
        return jsonify({"msg": "Bad username or password"}), 401

    l = fetch_license()

    saiku_rpc = SaikuRpcClient()
    resp = saiku_rpc.validate_license(l)

    j = json.loads(resp)

    if not j['valid']:
        return jsonify({"msg": "Invalid license"}), 401

    if not j['type'] == 'Saiku 4 Community Edition' and saiku_session.count_active_sessions() > j['numberOfUsers']:
        return jsonify({"msg": "Too many open sessions"})

    # Create a new session
    session_id = saiku_session.new_session(user)
    expires = datetime.timedelta(days=1)
    access_token = create_access_token(identity=session_id, expires_delta=expires)
    session_data = make_json_response(json.dumps({
        "language": "en",
        "sessionid": session_id,
        "isadmin": True,
        "authid": session_id,
        "roles": ["ROLE_ADMIN", "ROLE_USER"],
        "username": username,
        "access_token": access_token
    }))

    session_data.set_cookie(config['SESSION_ID_COOKIE'], value=session_id, path='/saiku', httponly=True)

    return session_data


def _fetch_session(session_id):
    data = {}
    if _is_logged_in():
        data = saiku_session.get_data(session_id)
    _validate_session(data)
    return make_json_response(json.dumps(data))


def _is_logged_in():
    session_id = get_jwt_identity()
    return session_id and saiku_session.exists(session_id)


def _validate_session(session):
    if (saiku_session.SESSION_ID not in session) or \
            (saiku_session.USERNAME not in session) or \
            (saiku_session.ROLES not in session):
        raise SaikuUnauthorizedAccessException()
    pass


def run(host=None, port=None, debug=None, load_dotenv=True, **options):
    app.run(host, port, debug, load_dotenv, **options)
