from saiku.database import UserDatabase

DEFAULT_AUTH_ENCODING = 'utf-8'

db = UserDatabase()


def authenticate(username, password):
    user = db.find_user_by_username(username)
    if user and db.verify_password(user.password, password):
        return user


def identity(payload):
    user_id = payload['identity']
    return db.find_user_by_user_id(user_id)
