from saiku.core.ee.license import fetch_license
from saiku.database import UserDatabase


def get_license_data():
    return fetch_license().to_json_data()


def get_users():
    return UserDatabase().to_json()
