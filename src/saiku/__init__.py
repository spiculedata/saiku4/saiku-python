import time
from urllib.error import URLError

from saiku.rpc.server import run, app
from saiku.core.datasources import ConnectionManager
from saiku.logging2 import get_logger
from threading import Thread
from urllib import request
from flask import g
from saiku.metrics.metrics import Metrics

_SERVER_WATCHER_DELAY_ = 0.2
_logger = get_logger('saiku.SaikuBootstrap')


class SaikuBootstrap(object):
    @staticmethod
    def init():
        global _logger

        _logger.debug('Initializing the connection manager ...')

        c = ConnectionManager()
        c.load()

    @staticmethod
    def start(host=None, port=None, debug=None, load_dotenv=True, **options):
        global _logger

        _logger.debug('Initializing Saiku 4 Server ...')

        SaikuBootstrap.init()

        # Thread function to check if the server has started
        def after_server_starts():
            index_url = 'http://' + (host or '127.0.0.1') + ':' + (str(port) or '5000')

            while True:
                try:
                    request.urlopen(url=index_url)
                    break
                except (ConnectionRefusedError, URLError) as e:
                    time.sleep(_SERVER_WATCHER_DELAY_)

            _logger.debug('Saiku 4 Server initialized and running at %s', index_url)

        # Setup a watching thread, to check for server initialization
        #Thread(target=after_server_starts).start()

        # Start server
        run(host, port, debug, load_dotenv, **options)


# Start the server
if __name__ == '__main__':
    SaikuBootstrap.start()
