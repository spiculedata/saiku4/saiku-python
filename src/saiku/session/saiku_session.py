import uuid
import time
import threading
from typing import Dict, Any
from saiku.config import config
from saiku.logging2 import get_logger

"""
Constants for common session keys
"""
USERNAME = "username"
ROLES = "roles"
AUTH_ID = "authid"
IS_ADMIN = "isadmin"
SESSION_ID = "sessionid"
LANGUAGE = "language"

"""
This module stores the session attributes in-memory.
Each session is identified by its key, and it also contains a last updated
variable, used to invalidate the session after a timeout, and a dictionary of
key-value pairs.
"""

_logger = get_logger('saiku.session.SaikuSession')


class SaikuSessionData(object):
    data: Dict[str, Any]
    last_updated: float

    def __init__(self, session_id):
        self.session_id = session_id
        self.data = dict()
        self.lock = threading.Lock()
        self.update()

    def put(self, key: str, value: Any):
        self.data[key] = value
        self.update()

    def get(self, key: str) -> Any:
        self.update()
        if key not in self.data:
            return None
        return self.data[key]

    def get_full_data(self) -> Any:
        self.update()
        return self.data

    def update(self):
        with self.lock:  # Acquire access to the thread lock to avoid race conditions
            self.last_updated = time.time()


# The dictionary that stores all active sessions
_sessions: Dict[str, SaikuSessionData] = dict()


def new_session(user):
    session_id = uuid.uuid1().hex

    data = SaikuSessionData(session_id)
    _sessions[data.session_id] = data
    _logger.debug('Created session - SESSION ID: %s', data.session_id)

    # After the session creation, fill it with the user data
    if user:
        put(session_id, LANGUAGE, user.language)
        put(session_id, SESSION_ID, session_id)
        put(session_id, IS_ADMIN, user.is_admin)
        put(session_id, AUTH_ID, session_id)
        put(session_id, ROLES, user.roles)
        put(session_id, USERNAME, user.username)

    return data.session_id


def delete_session(session_id: str):
    _sessions.pop(session_id, None)


def exists(session_id: str) -> bool:
    if session_id in _sessions:
        _sessions[session_id].update()
        return True
    return False


def put(session_id: str, key: str, value: Any):
    _sessions[session_id].put(key, value)


def get(session_id: str, key: str) -> Any:
    if session_id not in _sessions:
        return None
    return _sessions[session_id].get(key)


def get_data(session_id: str) -> Any:
    if session_id not in _sessions:
        return None
    return _sessions[session_id].get_full_data()


"""
Session Cleanup Thread
The sessions dictionary is kept clean by a thread that traverses all session 
data objects, removing those which are inactive for a time period bigger than
a timout.
"""
_SESSION_TIMEOUT_CHECK = 10  # interval, in seconds, between each run of the Session Cleanup Thread
_running = True


def _session_cleanup_thread():
    while _running:
        expired_sessions = []

        # Traverse the sessions dictionary looking for expired sessions
        for key, data in _sessions.items():
            current_time = time.time()
            with data.lock:  # Acquire access to the thread lock to avoid race conditions
                if current_time - data.last_updated > config['SESSION_TIMEOUT_IN_SECS']:
                    expired_sessions.append(key)  # It isn't allowed to remove values from dicts while looping over it

        # Loops over expired sessions list, removing each one of the sessions with the keys stored on it
        for key in expired_sessions:
            _logger.debug('Removing expired session - SESSION ID: %s', key)
            with _sessions[key].lock:
                del _sessions[key]

        time.sleep(_SESSION_TIMEOUT_CHECK)


def start_session_cleanup_thread():
    _logger.debug('Starting up the Session Cleanup Thread')
    cleanup_thread = threading.Thread(target=_session_cleanup_thread, args=(), daemon=True)
    cleanup_thread.start()


def stop_session_cleanup_thread():
    _logger.debug('Stopping the Session Cleanup Thread')
    global _running
    _running = False


def count_active_sessions():
    return len(_sessions)

"""
Main block intended for tests and demonstrating how this module works
"""
if __name__ == '__main__':
    print('Starting session cleanup thread')
    start_session_cleanup_thread()

    print('Creating a simple session')
    sid = new_session()
    print('Created session id: ' + sid)

    print('Storing some data in the session')
    put(sid, 'name', 'john doe')

    print('Retrieving the data')
    print('name: ' + get(sid, 'name'))

    print('Sleeping for a while')
    time.sleep(5)
    print('Trying to retrieve the data again')
    print('name: ' + str(get(sid, 'name')))
