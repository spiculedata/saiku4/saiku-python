import logging

_DEFAULT_LOGGING_LEVEL = logging.DEBUG
_DEFAULT_LOGGING_HANDLER = logging.StreamHandler()


def get_logger(prefix: str):
    _logger = logging.getLogger(prefix)
    _logger.setLevel(_DEFAULT_LOGGING_LEVEL)
    _logger.addHandler(_DEFAULT_LOGGING_HANDLER)
    return _logger
