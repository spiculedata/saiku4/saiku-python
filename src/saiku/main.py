#!/usr/bin/env python3

"""
Saiku 4 Backend Startup Script
"""

from saiku import SaikuBootstrap
from saiku.config import config

# Start the server
if __name__ == '__main__':
    SaikuBootstrap.start(host='0.0.0.0', port=config['SERVER_PORT'], threaded=False)
