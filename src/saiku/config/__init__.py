import os
import yaml

ROOT_DIR = os.path.abspath(
    os.path.join(
        os.path.join(os.path.join(
            os.path.dirname(os.path.abspath(__file__)),
            os.pardir),
            os.pardir),
        os.pardir))

# Initialize the config dictionary with default values
config = {
    'SERVER_PORT': 5000,
    'JWT_SECRET': 'super_secret',
    'SESSION_TIMEOUT_IN_SECS': 300,
    'REPO_DIR': 'repo',
    'DATA_DIR': 'data',
    'DEFAULT_ENCODING': 'iso-8859-15',
    'SESSION_ID_COOKIE': 'SESSION_ID_COOKIE',
    'UPLOAD_FOLDER': '../data'
}

try:
    # Try to load the config values from 'saiku.yaml' file
    config = yaml.safe_load(open(os.path.join('saiku', 'config', 'saiku.yaml')))
except OSError as e:
    pass  # Keep the default values instead
finally:
    config['REPO_DIR'] = os.path.join(ROOT_DIR, config['REPO_DIR'])
    config['DATA_DIR'] = os.path.join(ROOT_DIR, config['DATA_DIR'])
