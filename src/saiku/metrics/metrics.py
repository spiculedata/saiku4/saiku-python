import uuid
import multiprocessing
from psutil import virtual_memory
import sys
from pathlib import Path
import os
from influxdb import InfluxDBClient
import datetime
import threading

singleton = None


class Metrics:
    id = ""
    client = InfluxDBClient("metrics.meteorite.bi", 80, 'writer', '$sa!ku', 'startup')

    @staticmethod
    def get_instance():
        global singleton
        if not singleton:
            singleton = Metrics()
        return singleton

    def __init__(self):
        home = str(Path.home())
        if not os.path.exists(home+"/.saiku"):
            os.makedirs(home+"/.saiku")

        if not os.path.exists(home+"/.saiku/.id"):
            text_file = open(home+"/.saiku/.id", "w")
            text_file.write(str(uuid.uuid4()))
            text_file.close()

    def connect(self):
        self.client = InfluxDBClient("metrics.meteorite.bi", 80, 'writer', '$sa!ku', 'startup')

    def get_uuid(self):
        home = str(Path.home())
        with open(home+'/.saiku/.id', 'r') as file:
            data = file.read().replace('\n', '')
            self.id = data

    def startup(self):
        home = str(Path.home())
        with open(home+'/.saiku/.id', 'r') as file:
            data = file.read().replace('\n', '')
            self.id = data
            mem = virtual_memory()
            p = [
                {
                    "measurement": "startup",
                    "tags": {
                        "uuid": data
                    },
                    "fields": {
                        "ram": mem.total,
                        "cores": multiprocessing.cpu_count(),
                        "os": sys.platform
                    }
                }
            ]

            self.client.write_points(p)

    def post_point(self, point):
        self.client.write_points(point)

    def session(self, ua):
        self.get_uuid()
        return [
            {
                "measurement": "event",
                "tags": {
                    "type": "session"
                },
                "fields": {
                    "uuid": str(self.id),
                    "local_time": str(datetime.datetime.now().time()),
                    "user_agent": str(ua)
                }
            }
        ]

    def query(self, ua):
        self.get_uuid()
        return [
            {
                "measurement": "event",
                "tags": {
                    "type": "query"
                },
                "fields": {
                    "uuid": str(self.id),
                    "local_time": str(datetime.datetime.now().time()),
                    "user_agent": ua
                }
            }
        ]
        # return [
        #     {
        #         "measurement": "event",
        #         "tags": {
        #             "type": "query"
        #         },
        #         "fields": {
        #             "uuid": str(self.id),
        #             "local_time": datetime.datetime.now().time(),
        #             "user_agent": ua
        #         }
        #     }
        # ]

    def driver(self, driver):
        self.get_uuid()
        return [
            {
                "measurement": "event",
                "tags": {
                    "type": "driver"
                },
                "fields": {
                    "uuid": str(self.id),
                    "local_time": str(datetime.datetime.now().time()),
                    "driver": driver
                }
            }
        ]

if __name__ == '__main__':
        #testwrite()
    m = Metrics()
    m.startup()
    metrics = Metrics.get_instance()

    metrics.post_point(metrics.query("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36"))
    print(str(datetime.datetime.now().time()))
