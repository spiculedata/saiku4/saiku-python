import threading
import time
import logging
import asyncio
from aioinflux import InfluxDBClient
from pathlib import Path
from psutil import virtual_memory
import multiprocessing
import sys

logging.basicConfig(level=logging.DEBUG,
                    format='(%(threadName)-9s) %(message)s',)

point = {
    'time': '2009-11-10T23:00:00Z',
    'measurement': 'cpu_load_short',
    'tags': {'host': 'server01',
             'region': 'us-west'},
    'fields': {'value': 0.64}
}

class MyThread():
    client = None
    async def main(self):
        async with InfluxDBClient(host="metrics.meteorite.bi", port=81, username='writer', password='$sa!ku', db='startup') as client:
            #await client.create_database(db='testdb')
            await client.write(point)
            #resp = await client.query('SELECT value FROM cpu_load_short')
            #print(resp)
            print("Hello")




    def startup(self):
        home = str(Path.home())
        with open(home+'/.saiku/.id', 'r') as file:
            data = file.read().replace('\n', '')
            self.uuid = data
            mem = virtual_memory()
            p = [
                {
                    "measurement": "startup",
                    "tags": {
                        "uuid": data
                    },
                    "fields": {
                        "ram": mem.total,
                        "cores": multiprocessing.cpu_count(),
                        "os": sys.platform
                    }
                }
            ]

            self.client.write_points(p)

if __name__ == '__main__':
        t = MyThread()
        asyncio.get_event_loop().run_until_complete(t.main())
        print("Moved on")


