import asyncio
from aioinflux import InfluxDBClient

class MyThread():

    point = {
        'time': '2009-11-10T23:00:00Z',
        'measurement': 'cpu_load_short',
        'tags': {'host': 'server01',
                 'region': 'us-west'},
        'fields': {'value': 0.64}
    }

    blah = None;
    def __init__(self):
        self.blah="HELLO"

    async def fn1(self):
        print("CALLBACK")

    async def main(self):
        self.blah="HERE"
        print(self.blah)
        # async with InfluxDBClient(host="metrics.meteorite.bi", port=80, username='writer', password='$sa!ku', db='startup') as client:
        #     await client.create_database(db='testdb')
        #     await client.write(point)
        #     resp = await client.query('SELECT value FROM cpu_load_short')

    def getblah(self):
        return self.blah

#future = asyncio.ensure_future(main())
#print("waiting")
#future.add_done_callback(fn1)
t = MyThread()
loop = asyncio.new_event_loop()
task = asyncio.ensure_future(t.main(), loop=loop)
asyncio.set_event_loop(loop)
print("waiting")

loop.run_until_complete(task)
print("END")
print(t.getblah())
