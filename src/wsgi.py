#!/usr/bin/env python3

"""
Saiku 4 Backend Startup Script
"""


# Start the server
from saiku import SaikuBootstrap

if __name__ == '__main__':
    SaikuBootstrap.start()
