CREATE TABLE "dim_date" (
  "year_number" int(11) DEFAULT NULL,
  "month_number" int(11) DEFAULT NULL,
  "day_of_year_number" int(11) DEFAULT NULL,
  "day_of_month_number" int(11) DEFAULT NULL,
  "day_of_week_number" int(11) DEFAULT NULL,
  "week_of_year_number" int(11) DEFAULT NULL,
  "day_name" varchar(30) DEFAULT NULL,
  "month_name" varchar(30) DEFAULT NULL,
  "quarter_number" int(11) DEFAULT NULL,
  "quarter_name" varchar(2) DEFAULT NULL,
  "year_quarter_name" varchar(32) DEFAULT NULL,
  "weekend_ind" char(1) DEFAULT NULL,
  "days_in_month_qty" int(11) DEFAULT NULL,
  "date_sk" int(11) DEFAULT NULL,
  "day_desc" tinytext,
  "week_sk" int(11) DEFAULT NULL,
  "day_date" datetime DEFAULT NULL,
  "week_name" varchar(32) DEFAULT NULL,
  "week_of_month_number" int(11) DEFAULT NULL,
  "week_of_month_name" tinytext,
  "month_sk" int(11) DEFAULT NULL,
  "quarter_sk" int(11) DEFAULT NULL,
  "year_sk" int(11) DEFAULT NULL,
  "year_sort_number" varchar(4) DEFAULT NULL,
  "day_of_week_short_name" varchar(60) DEFAULT NULL
);
CREATE INDEX "i_date_year" ON "dim_date"("year_number");       
CREATE INDEX "i_date_month_num" ON "dim_date"("month_number");      
CREATE INDEX "i_date_month_name" ON "dim_date"("month_name");      
CREATE INDEX "i_date_dom" ON "dim_date"("day_of_month_number");      
CREATE INDEX "i_date_key" ON "dim_date"("date_sk");
CREATE TABLE "earthquakes" (
  "time" int(11) DEFAULT NULL,
  "latitude" double DEFAULT NULL,
  "longitude" double DEFAULT NULL,
  "depth" double DEFAULT NULL,
  "mag" double DEFAULT NULL,
  "magType" varchar(14) DEFAULT NULL,
  "nst" bigint(20) DEFAULT NULL,
  "gap" double DEFAULT NULL,
  "dmin" tinytext,
  "rms" double DEFAULT NULL,
  "net" varchar(20) DEFAULT NULL,
  "id" varchar(50) DEFAULT NULL,
  "place" varchar(250) DEFAULT NULL,
  "id2" int(11) NOT NULL,
  PRIMARY KEY ("id2")
);
CREATE INDEX "i_fact_time" ON "earthquakes"("time");       
CREATE INDEX "i_fact_lat" ON "earthquakes"("latitude");       
CREATE INDEX "i_fact_lon" ON "earthquakes"("longitude");       
CREATE INDEX "i_fact_depth" ON "earthquakes"("depth");       
CREATE INDEX "i_fact_mag" ON "earthquakes"("mag");       
CREATE INDEX "i_fact_net" ON "earthquakes"("net");       
CREATE INDEX "i_fact_id" ON "earthquakes"("id");       
CREATE INDEX "i_fact_place" ON "earthquakes"("place");       
